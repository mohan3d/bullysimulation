package com.c3s.messages;

public class TaskResultMessage  extends Message{
    public TaskResultMessage(int processUid) {
        super(processUid, MessageType.TASK_RESULT);
    }
}
