package com.c3s.messages;


public enum MessageType {
    CHECK,
    OK,
    ELECTIONS,
    NO_HIGHER,
    VICTORY,
    TASK,
    TASK_RESULT
}
