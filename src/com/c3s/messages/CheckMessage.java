package com.c3s.messages;

public class CheckMessage extends Message{
    public CheckMessage(int processUid) {
        super(processUid, MessageType.CHECK);
    }
}
