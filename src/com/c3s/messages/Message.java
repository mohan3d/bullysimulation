package com.c3s.messages;

import java.io.Serializable;

public abstract class Message implements Serializable {
    protected int processUid;
    protected MessageType messageType;

    public Message(int processUid, MessageType messageType) {
        this.processUid = processUid;
        this.messageType = messageType;
    }

    public int getProcessUid() {
        return processUid;
    }

    public void setProcessUid(int processUid) {
        this.processUid = processUid;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }
}
