package com.c3s.messages;

public class VictoryMessage extends Message{
    public VictoryMessage(int processUid) {
        super(processUid, MessageType.VICTORY);
    }
}
