package com.c3s.messages;

public class OkMessage extends Message {
    public OkMessage() {
        super(-1, MessageType.OK);
    }
}
