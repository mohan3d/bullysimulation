package com.c3s.messages;

public class ElectionsMessage extends Message{
    public ElectionsMessage(int processUid) {
        super(processUid, MessageType.ELECTIONS);
    }
}
