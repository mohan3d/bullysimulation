package com.c3s.messages;

public class NoHigherMessage extends Message{
    public NoHigherMessage() {
        super(-1, MessageType.NO_HIGHER);
    }
}
