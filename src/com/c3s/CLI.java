package com.c3s;


import java.util.Scanner;

public class CLI {
    private static Bully bully;

    private static void exit() {
        bully.stopSimulation();
        System.exit(0);
    }

    private static void help() {
        System.out.println("Supported commands:");
        System.out.println("start <process_id>");
        System.out.println("stop <process_id>");
        System.out.println("help");
        System.out.println("exit");
    }

    private static void stopProcess(int uid) {
        bully.stopProcess(uid);
    }

    private static void startProcess(int uid) {
        bully.startProcess(uid);
    }

    private static void execute(String command) {
        command = command.toLowerCase().trim();

        if (command.equals("help")) {
            help();
        } else if (command.equals("exit")) {
            exit();
        } else if (command.matches("(start|stop)\\s+\\d+")) {
            int uid = Integer.parseInt(command.split(" ")[1]);

            if (command.startsWith("stop")) {
                stopProcess(uid);
            } else {
                startProcess(uid);
            }
        } else {
            System.out.println("Invalid command.\n");
            help();
        }
    }

    public static void main(String[] args) {
        bully = new Bully(Settings.PROCESSES_COUNT);
        bully.startSimulation();

        Scanner sc = new Scanner(System.in);
        System.out.print(">> ");

        while (sc.hasNext()) {
            String command = sc.nextLine();
            execute(command);
            System.out.print(">> ");
        }
    }
}
