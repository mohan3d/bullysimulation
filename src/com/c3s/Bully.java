package com.c3s;

import com.c3s.messages.Message;
import com.c3s.messages.NoHigherMessage;
import com.c3s.messages.OkMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Bully {
    private final int PROCESSES_COUNT;
    private Process coordinator;
    private Process[] processes;

    private Server server;

    /**
     * Initializes the simulation fields before it starts,
     * Initializes list of n processes and the server object.
     *
     * @param processes_count representing count of processes in the simulation.
     */
    public Bully(int processes_count) {
        coordinator = null;

        PROCESSES_COUNT = processes_count;
        processes = new Process[PROCESSES_COUNT];

        for (int i = 0; i < PROCESSES_COUNT; ++i) {
            processes[i] = new Process(i);
        }

        server = new Server(this);
    }

    /**
     * Start the server and all processes threads.
     */
    public void startSimulation() {
        server.start();

        for (int i = 0; i < PROCESSES_COUNT; ++i) {
            processes[i].start();
        }
    }

    /**
     * Stops all the processes threads then stops the server.
     */
    public void stopSimulation() {
        for (int i = 0; i < PROCESSES_COUNT; ++i) {
            if (processes[i] != null) {
                processes[i].kill();
            }
        }

        server.kill();
    }

    /**
     * Stops specific process if 'uid' is a valid process id.
     *
     * @param uid id of the process to be stopped.
     */
    public void stopProcess(int uid) {
        if (uid < 0 || uid >= PROCESSES_COUNT) return;
        processes[uid].kill();
    }

    /**
     * Starts a specific process,
     * If the process Id is not valid do nothing,
     * If it's valid also the process is not alive then start it.
     *
     * @param uid id of the process to be started.
     */
    public void startProcess(int uid) {
        if (uid < 0 || uid >= PROCESSES_COUNT) return;
        if (processes[uid] == null || !processes[uid].isAlive()) {
            processes[uid] = new Process(uid);
            processes[uid].start();
        }
    }

    /**
     * Checks the incoming message type and responses based on it,
     * if the request needs to check if the coordinator is alive,
     * then new response will be sent with the state of the coordinator.
     * <p>
     * Supported message types (CHECK, ELECTIONS, VICTORY).
     *
     * @param message Message object received from a request.
     * @return Message object representing the response (chosen based on the request message).
     */
    public Message dispatchRequest(Message message) {
        int uid = message.getProcessUid();

        switch (message.getMessageType()) {
            case CHECK: {
                if (coordinator != null && coordinator.isAlive()) {
                    return new OkMessage();
                }
            }
            break;

            case ELECTIONS: {
                if (!initiateElections(uid)) {
                    return new NoHigherMessage();
                }
            }
            break;

            case VICTORY: {
                declareVictory(uid);
                return new OkMessage();
            }
        }

        return null;
    }

    /**
     * If there is existing coordinator then transfer the leadership,
     * from it to the new process (when there is a new process with higher id started),
     * otherwise set the new coordinator.
     *
     * @param uid id of the new coordinator.
     */
    private void declareVictory(int uid) {
        if (this.coordinator != null) {
            this.coordinator.transferCoordinator();
        }

        this.coordinator = processes[uid];
    }

    /**
     * Notifies all the alive higher processes to initiate new elections.
     *
     * @param uid id of the calling process.
     * @return true if there exists a higher process that can be the new coordinator
     * otherwise false indicating there is no higher process than the calling one.
     */
    private boolean initiateElections(int uid) {
        boolean higherRankExist = false;

        for (Process p : higherRankProcesses(uid)) {
            if (p.isAlive()) {
                higherRankExist = true;
                p.electionRequest();
            }
        }

        return higherRankExist;
    }

    /**
     * Creates a list with all processes with higher id than the caller process.
     *
     * @param uid id of the calling process.
     * @return list of all processes with higher id than uid.
     */
    private List<Process> higherRankProcesses(int uid) {
        return new ArrayList<>(
                Arrays.asList(processes).subList(uid + 1, PROCESSES_COUNT)
        );
    }
}
