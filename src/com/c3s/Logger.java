package com.c3s;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {
    /**
     * This class uses singleton pattern to create one instance,
     * which will be used across the processes to write to file.
     */
    private static final Logger instance = new Logger();
    private BufferedWriter bufferedWriter;

    private Logger() {
        try {
            bufferedWriter =
                    new BufferedWriter(new FileWriter(Settings.LOG_FILE_PATH, true));

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static Logger getInstance() {
        return instance;
    }

    public synchronized void log(String str) {
        try {
            bufferedWriter.write(str);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
