package com.c3s;

import com.c3s.messages.*;

import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Process extends Thread {
    private static final Logger logger = Logger.getInstance();

    private final int uid;

    private boolean alive;
    private boolean election;
    private boolean coordinator;

    private Socket socket;


    /**
     * Initializes election to true (So every process with start elections once it starts).
     * and sets the other variables to default values.
     *
     * @param uid id of the new process.
     */
    public Process(int uid) {
        this.uid = uid;
        this.alive = true;
        this.election = true;
        this.coordinator = false;
    }

    public int getUid() {
        return uid;
    }

    /**
     * Checks the state of the coordinator periodically if this process is not the coordinator it self,
     * Initiates new elections if election flag is set,
     * Notifies the other processes it's the new coordinator if there is no higher processes.
     */
    @Override
    public void run() {
        logger.log(logFormat("Started"));

        while (alive) {
            try {
                Thread.sleep((int) (Settings.MAX_SLEEP * Math.random()));

                if (coordinator || !alive) continue;

                if (election) {
                    if (initiateElections()) {
                        declareVictory();
                    }
                }

                // There is a problem with the coordinator.
                else if (!checkCoordinator()) {
                    electionRequest();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        logger.log(logFormat("Stopped"));
    }

    /**
     * Sets alive to false (there process is no longer alive),
     * coordinator to false (coordinator should be alive),
     * election to false (don't instantiate new elections util it's stopped).
     */
    public void kill() {
        this.alive = false;
        this.coordinator = false;
        this.election = false;
    }

    /**
     * Sets the election boolean to true, in order to initiate new elections as soon as possible.
     */
    public void electionRequest() {
        this.election = true;
    }

    /**
     * Sets the coordinator boolean to false, it's used in order to transfer the leadership to a new coordinator.
     */
    public void transferCoordinator() {
        this.coordinator = false;
    }

    /**
     * Starts new elections (after finding out that the coordinator is not responding).
     *
     * @return true if there is at least one higher process that can be the new coordinator before this process,
     * otherwise false.
     */
    private boolean initiateElections() {
        logger.log(logFormat("Initiating Elections"));
        boolean noHigherProcesses = false;

        connect();

        Message response = request(new ElectionsMessage(getUid()));

        if (response != null && response.getMessageType() == MessageType.NO_HIGHER) {
            noHigherProcesses = true;
        }

        this.election = false;

        disconnect();

        return noHigherProcesses;
    }

    /**
     * Creates request to check the coordinator.
     *
     * @return true if the coordinator is alive otherwise false.
     */
    private boolean checkCoordinator() {
        logger.log(logFormat("Checking coordinator"));
        connect();

        Message response = request(new CheckMessage(getUid()));
        boolean isOk = (response != null && response.getMessageType() == MessageType.OK);
        logger.log(logFormat("Coordinator is " + (isOk ? "alive" : "down")));

        disconnect();

        return isOk;
    }

    /**
     * Creates requests to the other processes in order to declare this process the new coordinator,
     * used after checking all higher rank process.
     */
    private void declareVictory() {
        logger.log(logFormat("Is the new coordinator"));
        connect();

        Message response = request(new VictoryMessage(getUid()));

        if (response.getMessageType() == MessageType.OK) {
            logger.log(logFormat("Notified the other processes"));
            this.coordinator = true;
        }

        disconnect();
    }

    /**
     * Creates new socket to connect to the server.
     */
    private void connect() {
        try {
            socket = new Socket(Settings.HOST, Settings.PORT);
            socket.setSoTimeout(Settings.TIMEOUT);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Closes the opened socket and set it to null.
     */
    private void disconnect() {
        try {
            socket.close();
            socket = null;
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Sends a request (Message) regardless it's type to the server and gets response (Message).
     *
     * @param message the message object to be sent.
     * @return Message object representing the response.
     */
    private Message request(Message message) {

        Message response = null;

        try {
            MessageHandler messageHandler = new MessageHandler(socket);

            messageHandler.send(message);
            response = messageHandler.receive();

            messageHandler.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return response;
    }

    /**
     * Creates new string in this format "Time, Thread#No: message",
     * using String.format method.
     *
     * @param msg String to be written to log file.
     * @return String in the needed format for log file.
     */
    private String logFormat(String msg) {
        return String.format("%s, Thread#%d: %s.",
                new SimpleDateFormat("[HH:mm:ss]").format(new Date()),
                getUid(),
                msg
        );
    }
}
