package com.c3s;

public class Settings {
    /**
     * This class defines global variables used by (server, process and the logger) classes.
     */
    public static final int PORT = 8888;
    public static final int MAX_SLEEP = 5000;
    public static final int TIMEOUT = 1000;
    public static final int PROCESSES_COUNT = 4;
    public static final String HOST = "localhost";
    public static final String LOG_FILE_PATH = "log.txt";
}
