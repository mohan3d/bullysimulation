package com.c3s;


import com.c3s.messages.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class MessageHandler {
    private ObjectInputStream in;
    private ObjectOutputStream out;

    public MessageHandler(Socket socket) {
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close reader/writer streams.
     *
     * @throws IOException may be thrown by one of the streams.
     */
    public void close()
            throws IOException {
        in.close();
        out.close();
    }

    /**
     * Reads message from socket.
     *
     * @return the incoming message.
     * @throws IOException            may be thrown by inputStream.
     * @throws ClassNotFoundException may be thrown if incoming message has no class.
     */
    public Message receive()
            throws IOException, ClassNotFoundException {
        return (Message) in.readObject();
    }

    /**
     * Sends message through socket.
     *
     * @param message the message object to be sent.
     * @throws IOException may be thrown by outputStream.
     */
    public void send(Message message)
            throws IOException {
        out.writeObject(message);
        out.flush();
    }
}
