package com.c3s;

import com.c3s.messages.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {
    private boolean serve;

    private Bully bully;

    /**
     * Initializes serve to true it's used to stop the server.
     *
     * @param bully Bully object will be used to dispatch incoming messages.
     */
    public Server(Bully bully) {
        this.serve = true;
        this.bully = bully;
    }

    /**
     * This method serves until it's requested to stop,
     * when there is an incoming message it uses process method to handle it.
     */
    @Override
    public void run() {

        Socket socket;

        try (ServerSocket serverSocket = new ServerSocket(Settings.PORT)) {
            while (serve) {
                socket = serverSocket.accept();
                socket.setSoTimeout(Settings.TIMEOUT);
                process(socket);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Sets serve to false in order to stop this thread.
     */
    public void kill() {
        serve = false;
    }

    /**
     * Receives message from the socket, and responds to it.
     * Bully dispatch is used to return the right response for incoming message.
     *
     * @param socket Socket object representing incoming connection.
     */
    private void process(Socket socket) {
        try {

            MessageHandler messageHandler = new MessageHandler(socket);
            Message message = messageHandler.receive();

            Message response = bully.dispatchRequest(message);
            messageHandler.send(response);

            socket.close();
            messageHandler.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
